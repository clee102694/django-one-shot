from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
# Create your views here.


def todo_list_list(request):
    todolist = TodoList.objects.all()
    context = {
        "todolist": todolist,
    }
    return render(request, "todos/todos.html", context)


def todo_list_detail(request, id):
    detail = get_object_or_404(TodoList, id=id)
    context = {
        "detail": detail
    }
    return render(request, "todos/detail.html", context)
